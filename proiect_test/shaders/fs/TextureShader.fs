#version 330 core
out vec4 FragColor;

in vec3 Normal;  
in vec2 TexCoords;
in vec3 FragPos;  
  
uniform sampler2D texture_diff;

uniform vec3 lightPos; 
uniform vec3 lightColor;
uniform vec3 viewPos;

void main()
{
   vec3 color = texture(texture_diff, TexCoords).rgb;

   vec3 ambient = 0.2f * lightColor;
   vec3 norm = normalize(Normal);
   vec3 lightDir = normalize(lightPos - FragPos);

   vec3 diff = 0.5f * max(dot(norm, lightDir), 0.0) * lightColor;

   vec3 viewDir = normalize(viewPos - FragPos);
   vec3 reflectDir = reflect(-lightDir, norm); 

   float spec = 1.0f * pow(max(dot(viewDir, reflectDir), 0.0), 4);

   vec3 result = max(spec, 0.2) * diff * color + ambient * color;
   FragColor = vec4(result, 1.0);
} 