#version 330 core
out vec4 FragColor;

in vec3 Normal;  
in vec3 vColor;
in vec3 FragPos;  

void main()
{
   FragColor = vec4(vColor, 1.0f);
} 