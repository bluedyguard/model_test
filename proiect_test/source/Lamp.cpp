#include <GL/glew.h>
#include <gtc/matrix_transform.hpp>

#include "Lamp.h"

Lamp::Lamp(const glm::vec3 &lightColor) {
	float vertices[] = {
	  -0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f,  0.02f, -0.02f,
	   0.02f,  0.02f, -0.02f,
	  -0.02f,  0.02f, -0.02f,
	  -0.02f, -0.02f, -0.02f,

	  -0.02f, -0.02f,  0.02f,
	   0.02f, -0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f,  0.02f,
	  -0.02f, -0.02f,  0.02f,

	  -0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f, -0.02f,
	  -0.02f, -0.02f, -0.02f,
	  -0.02f, -0.02f, -0.02f,
	  -0.02f, -0.02f,  0.02f,
	  -0.02f,  0.02f,  0.02f,

	   0.02f,  0.02f,  0.02f,
	   0.02f,  0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,

	  -0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f,  0.02f,
	   0.02f, -0.02f,  0.02f,
	  -0.02f, -0.02f,  0.02f,
	  -0.02f, -0.02f, -0.02f,

	  -0.02f,  0.02f, -0.02f,
	   0.02f,  0.02f, -0.02f,
	   0.02f,  0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f, -0.02f
	};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindVertexArray(VAO);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	this->lightColor = lightColor;
	lightPosition = glm::vec3(0.0f);
}

void Lamp::draw(Shader &shader) {
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, lightPosition);
	shader.setMat4("model", model);
	shader.setVec3("lightColor", lightColor);

	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void Lamp::setLightPosition(const glm::vec3 &newPosition) {
	lightPosition = newPosition;
}

const glm::vec3& Lamp::getLightPosition() const {
	return lightPosition;
}

const glm::vec3& Lamp::getLightColor() const {
	return lightColor;
}