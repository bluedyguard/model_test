#include <GL/glew.h>
#include <gtc/matrix_transform.hpp>

#include "Shape.h"

Shape::Shape(std::vector<float> &vertices) {
	oXtranslationParameter = oYtranslationParameter = oZtranslationParameter = 0.0f;
	oXrotationParameter = oYrotationParameter = oZrotationParameter = scaleParameter = 0.0f;
	scale = oXtranslate = oYtranslate = oZtranslate = oXrotate = oYrotate = oZrotate = false;
	this->verticesSize = vertices.size();

	this->vertices = new float[vertices.size()];
	for (uint i = 0; i < verticesSize; ++i)
		this->vertices[i] = vertices[i];

	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, verticesSize * sizeof(float), this->vertices, GL_STATIC_DRAW);
	
	glBindVertexArray(this->VAO);
	// vertex position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);

	// vertex color
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));

	// vertex normal
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));

	glBindVertexArray(0);
}

Shape::Shape(std::vector<float> &vertices, std::vector<uint> &indices) {
	oXtranslationParameter = oYtranslationParameter = oZtranslationParameter = 0.0f;
	oXrotationParameter = oYrotationParameter = oZrotationParameter = scaleParameter = 0.0f;
	scale = oXtranslate = oYtranslate = oZtranslate = oXrotate = oYrotate = oZrotate = false;
	this->verticesSize = vertices.size();
	this->indicesSize = indices.size();

	this->vertices = new float[this->verticesSize];
	for (uint i = 0; i < verticesSize; ++i)
		this->vertices[i] = vertices[i];

	this->indices = new uint[this->indicesSize];
	for (uint i = 0; i < indicesSize; ++i)
		this->indices[i] = indices[i];

	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, verticesSize * sizeof(float), this->vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize * sizeof(uint), this->indices, GL_STATIC_DRAW);

	glBindVertexArray(this->VAO);
	// vertex position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);

	// vertex color
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));

	// vertex normal
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));

	glBindVertexArray(0);
}

Shape::~Shape() {
	delete[] vertices;
}

void Shape::draw(Shader &shader) {
	glm::mat4 model = glm::mat4(1.0f);
	if (scale) {
		scaleParameter += 0.05f;
		model = glm::scale(model, glm::vec3(glm::cos(glm::radians(scaleParameter))));
	}
	if (oXtranslate) {
		oXtranslationParameter += 0.05f;
		model = glm::translate(model, glm::vec3(1.0f, 0.0f, 0.0f) * glm::cos(glm::radians(oXtranslationParameter)));
	}
	if (oYtranslate) {
		oYtranslationParameter += 0.05f;
		model = glm::translate(model, glm::vec3(0.0f, 1.0f, 0.0f) * glm::cos(glm::radians(oYtranslationParameter)));
	}
	if (oZtranslate) {
		oZtranslationParameter += 0.05f;
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 1.0f) * glm::cos(glm::radians(oZtranslationParameter)));
	}
	if (oXrotate) {
		oXrotationParameter += 0.05f;
		model = glm::rotate(model, glm::radians(oXrotationParameter), glm::vec3(1.0f, 0.0f, 0.0f));
	}
	if (oYrotate) {
		oYrotationParameter += 0.05f;
		model = glm::rotate(model, glm::radians(oYrotationParameter), glm::vec3(0.0f, 1.0f, 0.0f));
	}
	if (oZrotate) {
		oZrotationParameter += 0.05f;
		model = glm::rotate(model, glm::radians(oZrotationParameter), glm::vec3(0.0f, 0.0f, 1.0f));
	}

	shader.setMat4("model", model);

	glBindVertexArray(this->VAO);
	if (indicesSize == 0)
		glDrawArrays(GL_TRIANGLES, 0, verticesSize / 9); // 9 (nr atribute pe linie de vertex)
	else 
		glDrawElements(GL_TRIANGLES, indicesSize, GL_UNSIGNED_INT, indices);
	glBindVertexArray(0);
}

void Shape::processKey(int key) {
	if (key == GLFW_KEY_U)
		oXrotate = !oXrotate;
	if (key == GLFW_KEY_I)
		oYrotate = !oYrotate;
	if (key == GLFW_KEY_O)
		oZrotate = !oZrotate;
	if (key == GLFW_KEY_J)
		oXtranslate = !oXtranslate;
	if (key == GLFW_KEY_K)
		oYtranslate = !oYtranslate;
	if (key == GLFW_KEY_L)
		oZtranslate = !oZtranslate;
	if (key == GLFW_KEY_P)
		scale = !scale;
}
