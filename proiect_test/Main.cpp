#include <GL/glew.h>

#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <glfw3.h>

#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

#include "Camera.h"
#include "Shader.h"
#include "Texture.h"
#include "Shape.h"
#include "Lamp.h"

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

GLuint ProjMatrixLocation, ViewMatrixLocation, WorldMatrixLocation;
Camera *pCamera = nullptr;

void Cleanup()
{
	delete pCamera;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// timing
int timerFrame = 0;
double deltaTime = 0.0f;    // time between current frame and last frame
double lastFrame = 0.0f;
float pollTimer = 0.0f;

// interact
int pressedKey = GLFW_KEY_UNKNOWN;
float lightRay = 2.0f;

int main()
{
	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Model test", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	glewInit();

	glEnable(GL_DEPTH_TEST);

	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0f, 2.0f, 5.0f));

	Shader basicShader("shaders/vs/BasicShader.vs", "shaders/fs/BasicShader.fs");
	Shader lampShader("shaders/vs/LampShader.vs", "shaders/fs/LampShader.fs");
	Shader shader("shaders/vs/Shader.vs", "shaders/fs/Shader.fs");

	std::vector<float> vertices = {
	//   -----position-----  --color(R,G,B)--  -----normal----

		 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 0
		 1.8f, 0.0f,  0.2f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 2
		 1.0f, 0.0f,  1.4f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 1

		 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 0
		 0.0f, 0.0f, -2.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 3
		 1.8f, 0.0f,  0.2f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 2

		 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 0
		-1.8f, 0.0f,  0.2f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // 4
		 0.0f, 0.0f, -2.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 3

		 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 0
		-1.0f, 0.0f,  1.4f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 5
		-1.8f, 0.0f,  0.2f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // 4

		 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 0
		 1.0f, 0.0f,  1.4f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 1
		-1.0f, 0.0f,  1.4f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f  // 5
	};

	std::vector<float> verticesUnique = {
		//   -----position-----  --color(R,G,B)--  -----normal----

			 0.0f, 0.0f,  0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 0
			 1.0f, 0.0f,  1.4f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 1
			 1.8f, 0.0f,  0.2f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 2
			 0.0f, 0.0f, -2.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // 3
			-1.8f, 0.0f,  0.2f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // 4
			-1.0f, 0.0f,  1.4f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f  // 5
	};

	std::vector<float> verticesTextured = {
		//   -----position----- -tex(U, V)-  -----normal----

			 0.0f, 0.0f,  0.0f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, // 0
			 1.0f, 0.0f,  1.4f, 0.7f, 0.0f, 0.0f, 1.0f, 0.0f, // 1
			 1.8f, 0.0f,  0.2f, 1.0f, 0.6f, 0.0f, 1.0f, 0.0f, // 2
			 0.0f, 0.0f, -2.0f, 0.5f, 1.0f, 0.0f, 1.0f, 0.0f, // 3
			-1.8f, 0.0f,  0.2f, 0.0f, 0.6f, 0.0f, 1.0f, 0.0f, // 4
			-1.0f, 0.0f,  1.4f, 0.3f, 0.0f, 0.0f, 1.0f, 0.0f  // 5
	};

	std::vector<uint> indices = {
		0, 2, 1,
		0, 3, 2,
		0, 4, 3,
		0, 5, 4,
		0, 1, 5
	};

	Shape shape(vertices), testEBO(verticesUnique, indices);
	Lamp lamp;

	float lampPositionAngle = 0.0f;

	// render loop
	while (!glfwWindowShouldClose(window)) {
		// per-frame time logic
		double currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		processInput(window);
		if(pollTimer > 0.0f)
			pollTimer -= deltaTime;
		else if (pressedKey != GLFW_KEY_UNKNOWN) {
			testEBO.processKey(pressedKey);
			pollTimer = 0.2f;
		}
		pressedKey = GLFW_KEY_UNKNOWN;

		// rendering
		glClearColor(0.0f, 0.05f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shader.use();
		shader.setMat4("view", pCamera->getViewMatrix());
		shader.setMat4("projection", pCamera->getProjectionMatrix());
		shader.setVec3("viewPos", pCamera->getPosition());
		shader.setVec3("lightPos", lamp.getLightPosition());
		shader.setVec3("lightColor", lamp.getLightColor());

		testEBO.draw(shader);

		lampShader.use();
		lampShader.setMat4("view", pCamera->getViewMatrix());
		lampShader.setMat4("projection", pCamera->getProjectionMatrix());

		lampPositionAngle += 0.05f;
		lamp.setLightPosition(glm::vec3(lightRay * glm::cos(glm::radians(lampPositionAngle)), lightRay * glm::sin(glm::radians(lampPositionAngle)), 0.0f));

		lamp.draw(lampShader);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	Cleanup();

	// glfw: terminate, clearing all previously allocated GLFW resources
	glfwTerminate();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		pCamera->processKeyboard(ECameraMovementType::FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		pCamera->processKeyboard(ECameraMovementType::BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		pCamera->processKeyboard(ECameraMovementType::LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		pCamera->processKeyboard(ECameraMovementType::RIGHT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		pCamera->processKeyboard(ECameraMovementType::UP, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
		pCamera->processKeyboard(ECameraMovementType::DOWN, deltaTime);
	
	if (glfwGetKey(window, GLFW_KEY_INSERT) == GLFW_PRESS)
		lightRay += 0.005;
	if (glfwGetKey(window, GLFW_KEY_DELETE) == GLFW_PRESS)
		lightRay -= 0.005;

	if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS)
		pressedKey = GLFW_KEY_U;
	if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS)
		pressedKey = GLFW_KEY_I;
	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
		pressedKey = GLFW_KEY_O;
	if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS)
		pressedKey = GLFW_KEY_J;
	if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
		pressedKey = GLFW_KEY_K;
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
		pressedKey = GLFW_KEY_L;
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
		pressedKey = GLFW_KEY_P;

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->reset(width, height);
	}
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and
	// height will be significantly larger than specified on retina displays.
	pCamera->reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	pCamera->mouseControl((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	pCamera->processMouseScroll((float)yOffset);
}