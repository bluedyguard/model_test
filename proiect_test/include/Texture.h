#pragma once

#define uint unsigned int

#include <string>
#include <iostream>

class Texture {
private:
	uint id;
	bool loaded = false;

public:
	void loadTexture(const std::string &filename);

	const uint& getId() const;
	bool isLoaded() const;

	static uint textureFromFile(const std::string &filename);
};
