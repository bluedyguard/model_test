#pragma once

#include <glfw3.h>
#include <vector>

#include "Shader.h"
#include "Texture.h"

#define uint unsigned int

class Shape {
private:
	uint VBO;
	uint VAO;
	uint EBO;
	float *vertices;
	uint *indices;
	uint verticesSize;
	uint indicesSize;
	Texture texture;

	float scaleParameter;
	float oXtranslationParameter;
	float oYtranslationParameter;
	float oZtranslationParameter;
	float oXrotationParameter;
	float oYrotationParameter;
	float oZrotationParameter;

	bool scale;
	bool oXtranslate;
	bool oYtranslate;
	bool oZtranslate;
	bool oXrotate;
	bool oYrotate;
	bool oZrotate;

public:
	Shape(std::vector<float> &vertices);
	Shape(std::vector<float> &vertices, std::vector<uint> &indices);
	Shape(std::vector<float> &vertices, const std::string &textureFilename);
	Shape(std::vector<float> &vertices, std::vector<uint> &indices, const std::string &textureFilename);
	~Shape();

	void draw(Shader &shader);
	void processKey(int key);
};