#pragma once

#define uint unsigned int

#include <glm.hpp>

#include "Shader.h"

class Lamp {
public:
	Lamp(const glm::vec3 &lightColor = { 1.0f, 1.0f, 1.0f });

	void setLightPosition(const glm::vec3 &newPosition);
	const glm::vec3& getLightPosition() const;
	const glm::vec3& getLightColor() const;

	void draw(Shader &shader);

private:
	uint VBO;
	uint VAO;
	glm::vec3 lightColor;
	glm::vec3 lightPosition;
};